import { toast } from 'react-toastify';

export const notify = (message, type) => {
  switch (type) {
    case 'success':
      toast.success(message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      break;
    case 'warning':
      toast.warning(message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      break;
    default:
      toast.error(message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      break;
  }
};