import { configureStore } from '@reduxjs/toolkit'
import { combineReducers } from 'redux'
import dishReducer from '../containers/Home/homeSlice'
const reducer = combineReducers({
    home: dishReducer,
  // here we will be adding reducers
})
const store = configureStore({
  reducer,
})
export default store;