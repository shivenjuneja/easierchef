import { Button, Card, Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { filterDishes, setInitialValues } from "./homeSlice";
import uniq from "lodash/uniq";
import Icon from "@material-ui/core/Icon";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme) => ({
  addCard: {
    width: 180,
    height: 180,
    border: "2px solid #1976d2",
    display: "flex",
    alignItems: "center",
    margin: "20px",
    boxShadow: "unset",
    justifyContent: "center",
    cursor: "pointer",
    ["@media (max-width:700px)"]: {
      margin: "20px auto",
    },
  },
  addText: {
    color: "#1976d2",
    margin: "0px 7px",
  },
  addIcon: {
    color: "#1976d2",
  },
  flexContainer: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    ["@media (min-width:701px) and (max-width:1024px)"]: {
      justifyContent: "space-between",
      padding: " 0px 40px",
    },
    ["@media (max-width:700px)"]: {
      display: "block",
    },
  },
  card: {
    width: 180,
    height: 180,
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    margin: "20px",
    justifyContent: "center",
    flexDirection: "column",
    boxShadow: "0px 0px 9px -5px #1976d2",
    ["@media (max-width:700px)"]: {
      margin: "20px auto",
    },
  },
  cardImage: {
    width: 80,
    height: 80,
    borderRadius: "50%",
  },
  cardText: {
    fontSize: "19px",
    margin: "8px 0px",
    fontWeight: "500",
  },
  submitButton: {
    marginLeft: 5,
    backgroundColor: "#1976d2",
  },
  chipContainer: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(0.5),
    },
  },
  clearButton: {
    backgroundColor: "#1976d2 !important",
    margin: "0 auto",
    display: "block",
  },
  heading: {
    fontSize: "35px",
    textAlign: "center",
    margin: "34px 0px",
    textTransform: "uppercase",
    fontWeight: 600,
  },
}));

const Home = ({ history }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setInitialValues());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const allDishes = useSelector((state) => state.home.allDishes);
  const [filters, setFilters] = useState([]);
  const [selectedFilter, setSelectedFilter] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    if (!filters.length) {
      let filterArr = [];
      allDishes.forEach((dish) => {
        filterArr = filterArr.concat(dish.ingredients.map((i) => i.name));
      });
      setFilters(uniq(filterArr));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allDishes]);

  const clearStorage = () => {
    localStorage.clear();
    dispatch(setInitialValues());
  };
  const onClickFilter = (name) => {
    const idx = selectedFilter.indexOf(name);
    if (idx > -1) {
      selectedFilter.splice(idx, 1);
    } else {
      selectedFilter.push(name);
    }
    dispatch(filterDishes(selectedFilter));
    setSelectedFilter([...selectedFilter]);
  };

  return (
    <Container fixed>
      <Typography
        className={classes.heading}
        gutterBottom
        variant="h2"
        component="h2"
      >
        Recipies
      </Typography>
      <div className={classes.chipContainer}>
        {filters?.length
          ? filters.map((name, idx) => (
              <Chip
                key={idx}
                label={name}
                variant={
                  selectedFilter.indexOf(name) > -1 ? "default" : "outlined"
                }
                className={classes.chip}
                onClick={() => onClickFilter(name)}
                color="primary"
              />
            ))
          : null}
      </div>

      <div className={classes.flexContainer}>
        <Card
          className={classes.addCard}
          onClick={() => history.push("/add-dish")}
        >
          <Icon className={classes.addIcon}>add_circle</Icon>
          <p className={classes.addText}>Add Recipies</p>
        </Card>
        {allDishes?.length ? (
          allDishes.map((dish, idx) => (
            <Card
              className={classes.card}
              key={idx}
              onClick={() => history.push("dish-detail", { dish })}
            >
              <img
                alt="dish"
                onError={(event) =>
                  (event.target.src =
                    "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/sticky-maple-roots-947d8f5.jpg?quality=90&resize=960,872")
                }
                src={
                  dish?.picture ||
                  "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/sticky-maple-roots-947d8f5.jpg?quality=90&resize=960,872"
                }
                className={classes.cardImage}
              />
              <p className={classes.cardText}>{dish?.name || ""}</p>
            </Card>
          ))
        ) : (
          <p style={{ alignSelf: "center" }}>No dishes added yet.</p>
        )}
      </div>
      <Button
        color="secondary"
        variant={"contained"}
        onClick={clearStorage}
        className={classes.clearButton}
      >
        Clear Storage
      </Button>
    </Container>
  );
};
export default Home;
