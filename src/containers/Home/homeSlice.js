import { createSlice } from "@reduxjs/toolkit";
import forEach from 'lodash/forEach';
import includes from 'lodash/includes';
import some from 'lodash/some';

export const homeSlice = createSlice({
  name: "home",
  initialState: {
    allDishes: [],
  },
  reducers: {
      setInitialValues: (state, action) => {
        state.allDishes = JSON.parse(localStorage.getItem("dishes")) || []
      },
    addDishes: (state, action) => {
      const data = [...state.allDishes, action.payload];
      localStorage.setItem("dishes", JSON.stringify(data));
      state.allDishes = data;
    },
    filterDishes: (state, action) => {
      let list = JSON.parse(localStorage.getItem("dishes")) || []
      let newArr = []
      if(action.payload.length){
        forEach(list, (item) => {
          some(item.ingredients, (ing) => {
            console.log(action.payload, ing.name)
            if(includes(action.payload, ing.name)){
              console.log('line 27')
              newArr.push(item);
              return true;
            }
          })
        })
      }else{
        newArr =JSON.parse(localStorage.getItem("dishes")) || []
      }

      state.allDishes = newArr;
    },
  },
  extraReducers: {},
});

export const { addDishes, filterDishes, setInitialValues } = homeSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`

export default homeSlice.reducer;
