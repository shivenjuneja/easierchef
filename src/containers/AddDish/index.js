import { Typography, Button, TextField, Container } from "@material-ui/core";
import { useDispatch } from "react-redux";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { notify } from "../../components/notify";
import { addDishes } from "../Home/homeSlice";
import { makeStyles } from "@material-ui/core/styles";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";

const LoginSchema = yup.object().shape({
  name: yup.string().required(),
  picture: yup.string().url(),
});
const useStyles = makeStyles({
  heading: {
    fontSize: "26px",
    margin: "18px 0px",
    textTransform: "uppercase",
    fontWeight: 600,
    textAlign: "center",
  },
  text: {
    fontSize: "15px",
    margin: "15px 0px",
    textTransform: "uppercase",
    fontWeight: 500,
    textAlign: "left",
  },
  input: {
    marginBottom: 10,
    width: "100%",
    marginRight: 10,
  },
  flexContainer: {
    display: "flex",
    ["@media (max-width:520px)"]: {
      display: "block",
    },
  },
  addCard: {
    color: "#1976d2",
    textAlign: "left",
    justifyContent: "flex-end",
    backgroundColor: "transparent !important",
  },
  innerText: {
    fontSize: "14px",
    margin: "15px 0px",
    fontWeight: 500,
    textAlign: "left",
    textTransform: "capitalize",
  },
  submitButton: {
    backgroundColor: "#1976d2 !important",
    color: "#fff",
    fontSize: "17px",
    fontWeight: 700,
    width: "40%",
    margin: "20px auto",
  },
  inputWrap: {
    display: "flex",
    alignItems: "center",
  },
  remove: {
    cursor: "pointer",
    color: "#d41b1b",
    textAlign: "right",
    display: "block",
  },
  innerContainer: {
    display: "flex",
    flexDirection: "column",
    maxWidth: "800px",
    margin: "0 auto",
    padding: "20px",
  },
  backWrap: {
    display: "flex",
    position: "absolute",
    left: "0px",
    cursor: "pointer",
    top: "4px",
    alignItems: "center",
    ["@media (min-width:701px) and (max-width:1024px)"]: {
      left: "10px",
    },
    ["@media (max-width:700px)"]: {
      left: "10px",
    },
  },
  backText: {
    color: "#1976d2",
    fontSize: "17px",
    fontWeight: 500,
    margin: "0px",
  },
  mainWrap: {
    position: "relative",
  },
  backIcon: {
    color: "#1976d2",
  },
});
const AddDish = ({ history }) => {
  const [steps, setSteps] = useState([{ value: "" }]);
  const dispatch = useDispatch();
  const [ingredients, setIngredients] = useState([
    { name: "", quantity: "", unit: "" },
  ]);
  const { register, handleSubmit, errors, setError } = useForm({
    validationSchema: LoginSchema,
    mode: "onChange",
  });
  const classes = useStyles();

  const onSubmit = (values) => {
    let err;
    if (!values?.name?.trim()?.length) {
      setError("name", { message: "Please add name" });
      // notify('Please add dish name');
      err = "name";
    }
    if (!values?.picture?.trim()?.length) {
      setError("picture", { message: "Please add picture" });
      // notify('Please add dish picture');
      err = "picture";
    }
    if (!steps.every((i) => i.value)) {
      notify("Please add all steps");
      return;
    }
    if (!ingredients.every((i) => i.name && i.quantity && i.unit)) {
      notify("Please add all ingredients");
      return;
    }
    if (err) return;
    const data = {
      name: values?.name,
      picture: values?.picture,
      ingredients,
      steps,
    };
    dispatch(addDishes(data));
    notify("Dish added successfully", "success");
    history.goBack();
  };

  const onAddIngredients = () => {
    setIngredients([...ingredients, { name: "", quantity: "", unit: "" }]);
  };

  const onIngredientsChange = (name, value, index) => {
    let data = [...ingredients];
    data[index][name] = value;
    setIngredients(data);
  };

  const onAddSteps = () => {
    setSteps([...steps, { value: "" }]);
  };

  const onDeleteIng = (index) => {
    const data = [...ingredients];
    data.splice(index, 1);
    setIngredients(data);
  };

  const onDeleteSteps = (index) => {
    const data = [...steps];
    data.splice(index, 1);
    setSteps(data);
  };

  const onStepChange = (value, index) => {
    let data = [...steps];
    data[index].value = value;
    setSteps(data);
  };

  return (
    <Container fixed className={classes.mainWrap}>
      <div className={classes.backWrap} onClick={() => history.goBack()}>
        <NavigateBeforeIcon className={classes.backIcon} />
        <Typography
          className={classes.backText}
          gutterBottom
          variant="p"
          component="p"
        >
          Back
        </Typography>
      </div>
      <Typography className={classes.heading} variant="h1" component="h2">
        Add new dish
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.innerContainer}>
          <div className={classes.flexContainer}>
            <TextField
              name="name"
              inputRef={register}
              label="Name"
              error={typeof errors?.name?.message === "string"}
              className={classes.input}
            />
            <TextField
              name="picture"
              inputRef={register}
              label="Picture URL"
              error={typeof errors?.picture?.message === "string"}
              className={classes.input}
            />
          </div>
          <Typography className={classes.text} variant="h5" component="h5">
            Steps
          </Typography>
          {steps.map((step, index) => (
            <div key={index} className={classes.inputWrap}>
              <TextField
                label={`Step ${index + 1}`}
                value={step.value}
                onChange={(e) => onStepChange(e.target.value, index)}
                className={classes.input}
              />
              {steps.length > 1 ? (
                <span
                  onClick={() => onDeleteSteps(index)}
                  className={`${classes.remove} material-icons`}
                >
                  delete
                </span>
              ) : null}
            </div>
          ))}

          <Button
            className={classes.addCard}
            type="button"
            onClick={onAddSteps}
          >
            + Add Steps
          </Button>
          <Typography className={classes.text} variant="h5" component="h5">
            Ingredients
          </Typography>
          {ingredients.map((step, index) => (
            <div key={index}>
              <Typography
                className={classes.innerText}
                variant="h5"
                component="h5"
              >
                Ingredient {index + 1}
              </Typography>
              <TextField
                label="Name"
                value={step.name}
                onChange={(e) =>
                  onIngredientsChange("name", e.target.value, index)
                }
                className={classes.input}
              />
              <TextField
                label="Quantity"
                value={step.quantity}
                onChange={(e) =>
                  onIngredientsChange("quantity", e.target.value, index)
                }
                className={classes.input}
              />
              <TextField
                label="Unit"
                value={step.unit}
                onChange={(e) =>
                  onIngredientsChange("unit", e.target.value, index)
                }
                className={classes.input}
              />
              {ingredients.length > 1 ? (
                <span
                  onClick={() => onDeleteIng(index)}
                  className={`${classes.remove} material-icons`}
                >
                  delete
                </span>
              ) : null}
            </div>
          ))}
          <Button
            className={classes.addCard}
            type="button"
            onClick={onAddIngredients}
          >
            + Add Ingredients
          </Button>
          <Button
            className={classes.submitButton}
            type="submit"
            color="secondary"
          >
            Submit
          </Button>
        </div>
      </form>
    </Container>
  );
};
export default AddDish;
