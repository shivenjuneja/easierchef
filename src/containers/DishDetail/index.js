import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import { Container } from "@material-ui/core";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: "20px auto",
    listStyleType: "inherit",
    borderRadius: "14px",
  },
  media: {
    height: 222,
    width: "100%",
  },
  stepText: {
    textAlign: "left",
    fontSize: "14px",
    textTransform: "uppercase",
    marginLeft: "-16px",
    fontWeight: 600,
  },
  listItem: {
    position: "relative",
    padding: "6px 18px",
    "&::before": {
      content: '""',
      width: "6px",
      height: "6px",
      backgroundColor: "#000",
      borderRadius: "50%",
      marginRight: "13px",
    },
  },
  innerCard: {},
  innerText: {
    margin: "0px",
  },
  listItemInner: {
    position: "relative",
    padding: "6px 18px",
    alignItems: "flex-start",
    lineHeight: "24px",
    "&::before": {
      content: '""',
      width: "6px",
      height: "6px",
      backgroundColor: "#000",
      borderRadius: "50%",
      marginRight: "13px",
      marginTop: 9,
    },
  },
  backWrap: {
    display: "flex",
    position: "absolute",
    left: "0px",
    cursor: "pointer",
    top: "4px",
    alignItems: "center",
    ["@media (min-width:701px) and (max-width:1024px)"]: {
      position: "unset",
      marginTop: '20px',
    },
    ["@media (max-width:700px)"]: {
      position: "unset",
      marginTop: '20px',
    },
  },
  backText: {
    color: "#1976d2",
    fontSize: "17px",
    fontWeight: 500,
    margin: "0px",
  },
  mainWrap: {
    position: "relative",
  },
  backIcon: {
    color: "#1976d2",
  },
});

const DishDetail = ({ history, location }) => {
  const dish = location?.state?.dish;
  const classes = useStyles();
  return (
    <Container fixed className={classes.mainWrap}>
      <div className={classes.backWrap} onClick={() => history.goBack()}>
        <NavigateBeforeIcon className={classes.backIcon} />
        <Typography
          className={classes.backText}
          gutterBottom
          variant="p"
          component="p"
        >
          Back
        </Typography>
      </div>
      <Card className={classes.root}>
        <CardActionArea>
          <img
            className={classes.media}
            src={
              dish?.picture ||
              "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/sticky-maple-roots-947d8f5.jpg?quality=90&resize=960,872"
            }
            onError={(event) =>
              (event.target.src =
                "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/sticky-maple-roots-947d8f5.jpg?quality=90&resize=960,872")
            }
            alt=""
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {dish?.name}
            </Typography>
            <List className={classes.innerCard}>
              <ListSubheader className={classes.stepText}>Steps</ListSubheader>
              {dish?.steps?.length
                ? dish.steps.map((dish, index) => (
                    <ListItem className={classes.listItem}>
                      {dish?.value}
                    </ListItem>
                  ))
                : undefined}
            </List>
            <List className={classes.innerCard}>
              <ListSubheader className={classes.stepText}>
                Ingredients
              </ListSubheader>
              {dish?.ingredients?.length
                ? dish?.ingredients?.map((dish, index) => (
                    <ListItem className={classes.listItemInner}>
                      {"Name: "}
                      {dish?.name}
                      <br />
                      {"Quantity: "}
                      {dish?.quantity} {dish?.unit}
                    </ListItem>
                  ))
                : undefined}
            </List>
          </CardContent>
        </CardActionArea>
      </Card>
    </Container>
  );
};
export default DishDetail;
