import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import RouteConfig, { routes } from './PrivateRoute';

export default function App() {
  return (
    <Router>
      <div>

        <Switch>
          {routes.map((route) => (
            <RouteConfig
              exact={route.exact}
              path={route.path}
              routeType={route.type}
              component={route.page}
              key={route.path}
              // currentUser={currentUser}
              // token={token}
              rightWrap={route.rightWrap}
              // isFetchingUser={isFetchingUser}
            />
          ))}
        </Switch>
      </div>
    </Router>
  );
}
