import React, { memo } from 'react';
import { Route } from 'react-router-dom';
import Home from '../containers/Home';
import AddDish from '../containers/AddDish';
import DishDetail from '../containers/DishDetail';

export const routes = [
  { path: '/', page: Home, type: 'guest', exact: true },
  { path: '/add-dish', page: AddDish, type: 'guest', exact: true },
  { path: '/dish-detail', page: DishDetail, type: 'guest', exact: true },
];
const RouteConfig = ({
  component: Component,
  routeType,
  rightWrap,
  token,
  currentUser,
  isFetchingUser,
  ...rest
}) => {
  if (!routeType) {
    return <Component {...rest} />;
  }

  return <Route {...rest} render={(props) => <Component {...props} />} />;
};

export default memo(RouteConfig);
